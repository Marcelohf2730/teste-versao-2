#!/bin/bash
CONTEXT="HOJE é atenção e correções"

LC_ALL=C
string="$CONTEXT"
length="${#string}"
char

for (( i = 0; i < length; i++ )); do
    char="${string:i:1}"
    if [[ "$char" == [a-zA-Z0-9.~_-] ]]; then
    DESCRIPTION+=$char 
    else    
    DESCRIPTION+=`printf '%%%02X' "'$char"`
    fi
done
echo $DESCRIPTION
