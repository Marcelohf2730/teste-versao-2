#!/bin/bash
JIRATOKEN="email:token"
GITLABTOKEN="token-here"
PROJECTJIRA=["board1" "board2"]

#Nomes dos projetos/boards no jira no arquivo jira
count=0
while read -r line;
do
   PROJECTJIRA[count]=`echo $line | sed "s/\r//g"`;
   #let count++  
   ((count++))
done < jira

#Busca todas as tasks e subtasks do jira dentro dos projetos informados no arquivo jira
for (( j=0; j<$count; j++ ))
do
    REQUEST=`curl GET -H "Authorization: Basic $JIRATOKEN" -H "Content-Type: application/json" https://<HOST-JIRA>/rest/api/2/search?jql=project%20%3D%20"${PROJECTJIRA[$j]}"`
    
    if  ! echo $REQUEST | grep "errorMessages"; then
        TASKS+=$REQUEST
    fi
done

#Busca o último milestone aberto
MILESTONE=`curl -X GET -H "PRIVATE-TOKEN: $GITLABTOKEN" "http://10.129.178.173:9080/api/v4/projects/36215864/milestones?state=active" | jq .[].id`

DATA=`date +'%F'`

#Seleciona as subtasks no json
SUBTASKS=`echo $TASKS | jq -r '.issues[] | select(.fields.issuetype.hierarchyLevel | contains(-1))'`

#Seleciona as subtasks não concluídas do jira no json
ISSUES=`echo $SUBTASKS | jq -r 'select(.fields.status.statusCategory.colorName | contains("blue-gray")) .key'`

for (( k=1; k>0; k++ ))
do
    NAMEISSUE=`echo $ISSUES | cut -f $k -d' '`
    
    #Busca a descrição das subtasks no Jira
    if  echo $NAMEISSUE | grep -P '\w'; then         
        
        TEXT=`curl GET -H "Authorization: Basic $JIRATOKEN" -H "Content-Type: application/json" https://<HOST-JIRA>/rest/api/2/issue/"$NAMEISSUE"?fields=description`
        CONTEXT=`echo $TEXT | jq .fields.description.content[].content[].text | sed "s/null//g" | cut -f 2 -d'"'`
        
        #Conversão do texto da descrição para formato aceito na URL
        LC_ALL=C
        length="${#CONTEXT}"
        DESCRIPTION=""
        for (( i = 0; i < length; i++ )); do
            char="${CONTEXT:i:1}"
            if [[ "$char" == [a-zA-Z0-9.~_-] ]]; then
            DESCRIPTION+=$char 
            else    
            DESCRIPTION+=`printf '%%%02X' "'$char"`
            fi
        done
        #Post das issues não repetidas no gitlab
        if ! curl -X GET -H "PRIVATE-TOKEN: $GITLABTOKEN" "http://10.129.178.173:9080/api/v4/projects/36215864/issues?state=opened" | grep "$NAMEISSUE"; then
            curl -X POST -H "PRIVATE-TOKEN: $GITLABTOKEN" "http://10.129.178.173:9080/api/v4/projects/36215864/issues?title="$NAMEISSUE"&<LABELS=X,Y>&milestone_id="$MILESTONE"&assignee_ids=<ID-USER>&due_date="$DATA"&description="$DESCRIPTION""
        fi
    else
        k=-443
    fi
done  


